"use strict";
const express        = require('express');
const MongoClient    = require('mongodb').MongoClient;
const bodyParser     = require('body-parser');
const app            = express();


var livereload = require('livereload');
var server = livereload.createServer();
server.watch(__dirname + "/public");
    let port;
    if (process.env.ENV === 'prod') {
        port = 80;
    } else {
        port = 3000;
    }
app.listen(port, () => {
    console.log('We are live on ' + port);
});


app.use(express.static('public'));
app.use('/assets', express.static('assets'));